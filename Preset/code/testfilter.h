/**************************************************************
WinFilter version 0.8
http://www.winfilter.20m.com
akundert@hotmail.com

Filter type: Band Pass
Filter model: Butterworth
Filter order: 1
Sampling Frequency: 25 KHz
Fc1 and Fc2 Frequencies: 1.000000 KHz and 1.100000 KHz
Coefficents Quantization: 16-bit

Z domain Zeros
z = -1.000000 + j 0.000000
z = 1.000000 + j 0.000000

Z domain Poles
z = 0.955322 + j -0.251132
z = 0.955322 + j 0.251132
***************************************************************/
int NCoef = 2;
int DCgain = 128;
int16_t iir(int16_t NewSample) {
    int16_t ACoef[NCoef+1];
    ACoef[NCoef+1]={30652, 0, -30652};



    int16_t BCoef[NCoef+1] = {
        16384,
        -31304,
        15986
    };

    static int32 y[NCoef+1]; //output samples
    //Warning!!!!!! This variable should be signed (input sample width + Coefs width + 2 )-bit width to avoid saturation.

    static int16_t x[NCoef+1]; //input samples
    int n;

    //shift the old samples
    for(n=NCoef; n>0; n--) {
       x[n] = x[n-1];
       y[n] = y[n-1];
    }

    //Calculate the new output
    x[0] = NewSample;
    y[0] = ACoef[0] * x[0];
    for(n=1; n<=NCoef; n++)
        y[0] += ACoef[n] * x[n] - BCoef[n] * y[n];

    y[0] /= BCoef[0];
    
    return y[0] / DCgain;
}

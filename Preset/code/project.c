#include "environment.h"
#include <stdlib.h>
#include "myfuncs.h"
#include "filter.h"

int16_t audio_0[1536000]; // stores 60 seconds of audio @ 25600 Hz, samples are 16 bit signed integers
int16_t audio_1[1536000];
int16_t audio_2[1536000];
int16_t audio_3[1536000];
int16_t audio_4[1536000];
int16_t audio_5[1536000];
int16_t audio_6[1536000];
int16_t audio_7[1536000];
int16_t audio_8[1536000];
int16_t audio_9[1536000];
uint32_t audio_length[10]; //the array entries store the amount of samples each file has

char *filepath_0 = "INPUT.WAV"; //direct filename of audiofiles located in the "project" folder
char *filepath_1 = "INPUT.WAV";
char *filepath_2 = "INPUT.WAV";
char *filepath_3 = "INPUT.WAV";
char *filepath_4 = "INPUT.WAV";
char *filepath_5 = "INPUT.WAV";
char *filepath_6 = "";
char *filepath_7 = "";
char *filepath_8 = "";
char *filepath_9 = "INPUT.WAV";

// THX you Internet for this filter, which is much better then ours:) 
// http://jaggedplanet.com/iir/iir-explorer.asp
uint32_t image[640][520]; // access: image[x][y], where image[0][0] ist the top left pixel
int samplerate = 25600;

int project(void) {
    //Preprocssing
    //HPF(audio_0, audio_1, audio_length[0]);
    
    int16_t max = get_max(audio_0, audio_length[0]);
    normalize(max, audio_0, audio_length[0], audio_1);
    save_audiofile("0_Normalized_.WAV", 1, audio_length[0]);
    int threshold = 1000;
    cut_word(audio_1, audio_2, audio_length[0], &audio_length[2], threshold);
    save_audiofile("0CUT_WORD.WAV", 2, audio_length[2]);
    for (int i =0; i< audio_length[0]; i++){
        audio_3[i]=0;
        audio_4[i]=0;
        audio_5[i]=0;
        audio_9[i]=0;
    }
    //new array


    //Men A
    for (uint32_t i = 0; i < audio_length[2]; ++i){
        audio_3[i]=applyfilter(audio_2[i], 1);
    } 

    for (uint32_t i = 0; i < audio_length[2]; ++i){
        audio_4[i]=applyfilter(audio_2[i], 2);
    } 

    for (uint32_t i = 0; i < audio_length[2]; ++i){
        audio_5[i]=applyfilter(audio_2[i], 3);
    }
    printf("Bub");
    save_audiofile("0_manfilter.wav", 3, audio_length[0]);
    save_audiofile("0_womanfilter.wav", 4, audio_length[1]);
    save_audiofile("0_childfilter.wav", 5, audio_length[1]);

    int16_t maxM = get_max(audio_3, audio_length[3]);
    int16_t maxW = get_max(audio_4, audio_length[4]);
    int16_t maxC = get_max(audio_5, audio_length[5]);
    int16_t maxCut = get_max(audio_2, audio_length[2]);
    printf("Die Max Werte sind: %d vor dem Filtern, Maenner: %d, Frauen: %d, Kinder: %d\n", maxCut, maxM, maxW, maxC);
    if(maxM > maxW && maxM > maxC ){
        printf("Mann\n");
        audio_9[0]=1;
    }
    else if(maxW > maxM && maxW > maxC ){
        printf("Frau\n");
        audio_9[0]=2;
    }
    else if(maxC> maxM && maxC > maxW ) {
        printf("Kind\n");
        audio_9[0]=3;
    }
    else  {audio_9[0]=2;}

    save_audiofile("preset.wav", 9, audio_length[1]);

    return 0;
}   
#include <stdlib.h>
#include "environment.h"

void cut_frames (int framesize, int16_t length, int16_t audio_0[], int16_t audio[]){
    for (int i = framesize; i <= audio_length[0] - framesize; i++){
    audio[i-framesize]= audio_0[i];
    }
}

void absolute(int16_t audio[], uint32_t length){
    for (uint32_t i = 0; i < length; ++i){
        audio[i] = abs(audio[i]); 
    }
    return;   
}
    
int16_t get_max(int16_t audio[], uint32_t length){
    int16_t maxim = 0;
    for (uint32_t i = 0; i < length; ++i){
        if(maxim< audio[i]){
            
            maxim= audio[i];
        } 
    }
    return maxim; 
}

void copy_array(int16_t from[], int16_t to[], uint32_t length){
     for (uint32_t i = 0; i < length; ++i){
        to[i] = from[i];
     }
}

void normalize (int16_t max, int16_t audio[], uint32_t length, int16_t norm_int_array[]){
    float float_norm[length];
    for (uint32_t i = 0; i < length; ++i){
        float_norm[i]= (float)audio[i];
    }    
    // Nomralisieren mit absolut
    float factor = 32767.f/ (float)max; 
    for (uint32_t i = 0; i < length; ++i){
        float blub = (float)float_norm[i];
        float z = blub*factor;
        float_norm[i] = z;
    }
    // float array to int array
    for (uint32_t i=0; i < length; i++){
        norm_int_array[i]= (int16_t)float_norm[i];
    }
}

void get_envelope (int max, int framesize, int16_t env_length, int16_t env[], int16_t cut_array[], int16_t audio[]){
    for (uint32_t counter = 0; counter < env_length; counter++){
        int16_t maximal= 0;
        for (uint32_t i = 0 + framesize * counter; i < framesize*(counter + 1); ++i){
            if(maximal < audio[i]){
                maximal= audio[i];
            }
        }
        //printf("Max of Frame: %d \n", maximal);
        env[counter] = maximal;
        //printf("%d\n", maximal);
    }
    
    int threshold = max * 0.1;
    //printf("env length %d\n",env_length);
    for (int i = 0; i < env_length; i++){
        cut_array[i] = 0;
    }

    //int counter3 = 0;
    for (int i = 0; i < env_length; i++){
        if (env[i] > threshold /*&& env[i + 2] > threshold*/){
            cut_array[i] = env[i];   
        }
    }   

    /*
    for (int i = 0 ; i < env_length; i++){
        printf("cut array: %d \n", cut_array[i]);
    }
*/    
}

void cut_word(int16_t audio_array[], int16_t cut_word[], uint32_t length, uint32_t *newlength, int threshold){
    int max = get_max(audio_array, length);
    //printf ("max is %d\n", max);

    int framesize = 512;
    int env_length = length/framesize;
    int16_t env[env_length];
    int16_t cut_array[env_length];
    get_envelope(max, framesize, env_length, env, cut_array, audio_array);

    int iterator;
    for (iterator = 0; iterator < env_length; iterator++){
        if (cut_array[iterator]  && cut_array[iterator + 1] && cut_array[iterator + 6] > 0){
            break;
        }
    }

    //printf ("%d\n", iterator);

    int iteratorend;
    for (iteratorend = 0; iteratorend < env_length; iteratorend++){
        if (cut_array[iteratorend-6] > 0 && cut_array[iteratorend - 1] > 0  && cut_array[iteratorend - 2] > 0 && cut_array[iteratorend] == 0){
            break;
        }
    } 
    //printf ("iterator end: %d\n", iteratorend);
    for(int i=0; i< *newlength; i++){
        cut_word[i]=0;
    }
    int wordbeg = framesize * iterator;
    int wordend = framesize * iteratorend;
    *newlength = wordend - wordbeg;
    

    for(int i = wordbeg; i < wordend; i++){
        cut_word[i-wordbeg] = audio_array[i];
        //printf("cut word %d \n", cut_word[i-wordbeg]);
    }

}

void make_image(uint32_t length, int16_t audio[], char* filename){
    for(int y=0; y<520; y++)
      for(int x=0; x<640; x++)
        set_pixel(x, y, 0, 0, 255);  // blue
    
    int16_t x_axis[640];
    for(int idx=0; idx<640; idx++)
      x_axis[idx] = 0;
    
    for(int t=0; t<length; t++) {
       int idx = t * 640 / length;
       int sample = audio[t];
       x_axis[idx] = sample;
    }
    for(int x=0; x<640; x++) {
      int level = 519 - x_axis[x] / 64;
      for(int y=level; y<520; y++)
      {
      set_pixel(x, y, 255, 255, 0); // yellow level on blue screen
      }
    //printf("Level value %d \n", level);
    }

    save_imagefile(filename);
    
}

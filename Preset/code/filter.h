typedef double REAL;
#define NBQ 2
REAL xyv[]={0,0,0,0,0,0,0,0,0};

REAL applyfilter(REAL v, int preset)
{
	REAL biquada[4]={0,0,0,0};
	REAL biquadb[4]={0,0,0,0};
	REAL gain=0;
	if( preset == 1){
		//Man A, Cutoff: 730 Hz, width: 100 Hz
		biquada[0] = 0.9835781848181744;
		biquada[1] = -1.9548991443005428;
		biquada[2] = 0.9819320874122156;
		biquada[3] = -1.947132229495664;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=6724.453312737981;

	}
	if (preset == 2){
		//woman A, Cutoff: 850 Hz, width: 100 Hz
		biquada[0] = 0.9834684293158923;
		biquada[1] = -1.9440319084303503;
		biquada[2] = 0.9820627820893103;
		biquada[3] = -1.9355171545283592;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=6652.112391604954;
		
	}
	if (preset == 3){
		//Child A, 1030Hz, Width: 100
		biquada[0] = 0.9833499991378313;
		biquada[1] = -1.9246203438746412;
		biquada[2] = 0.982199954481513;
		biquada[3] = -1.9148895759748172;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=6609.455993094924;
	}

	if (preset == 4){

		
	}
	if (preset == 5){

	}
	if (preset == 6){

	}
	if( preset == 7){
		//Man I, F2, 2290 Hz, Width: 300
		biquada[0] = 0.9512270752259476;
		biquada[1] = -1.6781880566897773;
		biquada[2] = 0.9471116701381416;
		biquada[3] = -1.6204882514641183;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=773.4728330526677;
	}
	if (preset == 8){
		
	}
	if (preset == 9){
		
	}
	if (preset == 10){
		
	}
	if (preset == 11){

	}
	if (preset == 12){
		
	}
	int i,b,xp=0,yp=3,bqp=0;
	REAL out=v/gain;
	for (i=8; i>0; i--) {xyv[i]=xyv[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv[xp]=out;
		for(i=0; i<len; i++) { out+=xyv[xp+len-i]*biquadb[bqp+i]-xyv[yp+len-i]*biquada[bqp+i]; }
		bqp+=len;
		xyv[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}
@echo off
cd /d "C:\Program Files\Webots\msys64\mingw64\bin\"
gcc.exe -Wall -O0 "%~dp0code\*.c" -o "%~dp0project\_project.exe" -D PC
echo.
echo Press [F] To Run Project...
choice /t 3 /c Fabcdeghijklmnopqrstuvwxyz1234567890 /d a > nul
if %errorlevel%==1 goto run_project
goto end

:run_project
cd "%~dp0project"
cls
_project.exe

:end
echo Done...

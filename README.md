# A I S Detection

## Description
Low level approach for human sound detection. ATM detects wether the speaker has a high or low voice and then it detects "A", "I"  "S" sounds in a word and finds it position. 
Therefore words like "Salami" can be detected.

## Demo (Windows only)
- Clone the repo
- Make sure your Mic is available
- Open Demo/ScriptExecuter.exe 
- Select MYONTALK.zip
- Check "AUTO" Button
- Have fun! 

## Authors and acknowledgements
This project was created during the study of humoid robotic at the University of Applied Sciences BHT.
The ScriptExecuter.exe was developed by Prof. Manfred Hild.
The code for the detection was developed by Patricia Waldmann and myself. 

## License
MIT license
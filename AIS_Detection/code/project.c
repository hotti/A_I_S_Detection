#include "environment.h"
#include <stdlib.h>
#include "myfuncs.h"
#include "filter.h"
#include <stdbool.h>

int16_t audio_0[1536000]; // stores 60 seconds of audio @ 25600 Hz, samples are 16 bit signed integers
int16_t audio_1[1536000];
int16_t audio_2[1536000];
int16_t audio_3[1536000];
int16_t audio_4[1536000];
int16_t audio_5[1536000];
int16_t audio_6[1536000];
int16_t audio_7[1536000];
int16_t audio_8[1536000];
int16_t audio_9[1536000];
uint32_t audio_length[10]; //the array entries store the amount of samples each file has

char *filepath_0 = "INPUT.WAV"; //direct filename of audiofiles located in the "project" folder
char *filepath_1 = "INPUT.WAV";
char *filepath_2 = "INPUT.WAV";
char *filepath_3 = "preset.wav";
char *filepath_4 = "";
char *filepath_5 = "INPUT.wav";
char *filepath_6 = "";
char *filepath_7 = "";
char *filepath_8 = "";
char *filepath_9 = "";

// THX you Internet for this filter, which is much better then ours:) 
// http://jaggedplanet.com/iir/iir-explorer.asp
uint32_t image[640][520]; // access: image[x][y], where image[0][0] ist the top left pixel
int samplerate = 25600;

int project(void) {

    //Preprocssing
    //new array
    uint32_t length = audio_length[3];
    int16_t presets[2];
    if (audio_3[0] == 1){
        presets[0] = 1;
        presets[1] = 7;
    }
    
    if (audio_3[0] == 2){
        presets[0] = 2;
        presets[1] = 8;
    }
    if (audio_3[0] == 3){
        presets[0] = 3;
        presets[1] = 9;
    }

    //preprocessing
    int16_t maxUnfiltered = get_max(audio_0, audio_length[0]);
    HPF(audio_0, audio_5, audio_length[5]);
    normalize(maxUnfiltered, audio_5, audio_length[5], audio_5);
    char *img_name3 = "000_norm.PPM";
    make_image(audio_length[5], audio_5, img_name3);
    save_audiofile("0Normalized.WAV", 5, audio_length[5]);
    int thres = 100;
    cut_word(audio_5, audio_1, audio_length[5], &audio_length[1], thres);
    save_audiofile("0CUT_WORD.WAV", 1, audio_length[1]);
    for (int i =0; i< audio_length[1]; i++){
        audio_2[i]=audio_1[i];
        audio_3[i]=audio_1[i];
        audio_4[i]=audio_1[i];  
    }


    for (int i = 0; i < audio_length[1]; ++i){
        audio_2[i]=applyfilter(audio_2[i], presets[0]);
        //printf("%d", audio_0[i]);
    } 
    save_audiofile("0A.wav", 2, audio_length[1]);
    char *img_name2 = "000_PICA.PPM";
    make_image(audio_length[1], audio_2, img_name2);

    for (int i = 0; i < audio_length[1]; ++i){
        audio_3[i]=0.5*applyfilter(audio_3[i], 10);
        //printf("%d", audio_0[i]);
    } 
    /*
    for (uint32_t i = 0; i < audio_length[1]; ++i){
        audio_3[i]=100*bandpassI(audio_3[i]);
    }
    */
    char *img_name4 = "000_PICI.PPM";
    make_image(audio_length[1], audio_3, img_name4);

    //HPF(audio_2, audio_2, audio_length[1]);

    
    save_audiofile("0I.wav", 3, audio_length[1]);

    int positiona = 0;
    int a_index = max_index(audio_2, audio_length[1], positiona);
    

    int positioni = 0;
    int i_index = max_index(audio_3, audio_length[1], positioni);

    // s detection
  
    length = audio_length[1];
     
    int16_t audio_s[length];
     
    for(int i= 0; i<length;i++){
        audio_s[i] = audio_1[i];
    }
   printf("here %d\n", length);
    int framesize = 512;
    int16_t list_null_counter[length-framesize];
      printf("here");
    int16_t null = 0;
    // get Nulldurchgänge
    for (int sample = 0; sample < (length-framesize); sample++){
        list_null_counter[sample] =0;
        null = 0;
        for(int count = sample; count<(sample+framesize); count++){
            if ((audio_s[count] <= 0) && (audio_s[count+1] > 0)){
              null += 1;
              }
        }
        list_null_counter[sample]=null;
    }
   
    //get first index of highest s
    int16_t zisch_max = get_max(list_null_counter, length-framesize);
    int z_index=0; 
    for(z_index; z_index<(length-framesize); z_index++){
        if(list_null_counter[z_index] == zisch_max){
            break;
        }
    }

   
    printf("z_index %d \n", z_index);
    printf("i_index = %d\n", i_index);
    printf("a_index = %d\n", a_index);
    char *img_name = "000_PIC.PPM";
    make_image(length-framesize, list_null_counter, img_name);
    
    // check if z-detection is valid
    //bool isZisch = false; 
    int zisch_energy = 0; 
    int zisch_energy_length = 10; 
    for (int i = 0; i < zisch_energy_length; i++){
        zisch_energy += list_null_counter[z_index - zisch_energy_length/2 + i];
    }
    //printf("Zisch_ ernergy is %d\n", zisch_energy);

    bool isZisch = false; 
    int zisch_thresh = 500;
    if (zisch_energy > zisch_thresh){
        isZisch = true;
    }
    printf("%d\n", isZisch);

    int returnvalue = category(z_index, a_index, i_index, isZisch);
    printf("return value ist: %d\n", returnvalue);
return returnvalue; // or 'b'
}   
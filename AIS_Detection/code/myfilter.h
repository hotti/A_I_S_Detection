#include <stdlib.h>
#include "environment.h"
//#include <math.h>
#define M_PI
//Wie man AmplitudenEnvelope berechnet: https://www.youtube.com/watch?v=rlypsap6Wow 

/*
Notizen: 
-Tiefpass Filter unter 100 Hz, um störungen weg zu filtern
	-fade in filter
	-erste 700 samples weggeschnitten
	-für a und i: pseudo inverse
	-boosting (ader)
*/

//HP1000
typedef double REALHP1KHZ;
#define NBQ 2
REALHP1KHZ biquada[]={0.8057442364623504,-1.732928010636769,0.5876634687678297,-1.5236413003220304};
REALHP1KHZ biquadb[]={1,-2,1,-2};
REALHP1KHZ gain=1.4532391459828473;
REALHP1KHZ xyv[]={0,0,0,0,0,0,0,0,0};

REALHP1KHZ HP1000(REALHP1KHZ v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REALHP1KHZ out=v/gain;
	for (i=8; i>0; i--) {xyv[i]=xyv[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv[xp]=out;
		for(i=0; i<len; i++) { out+=xyv[xp+len-i]*biquadb[bqp+i]-xyv[yp+len-i]*biquada[bqp+i]; }
		bqp+=len;
		xyv[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}

///HP2000 
typedef double REALHP2KHZ;
#define NBQ 2
REALHP2KHZ biquada1[]={0.6577189485890346,-1.3957215810419235,0.3346859143774704,-1.1237429216785333};
REALHP2KHZ biquadb1[]={1,-2,1,-2};
REALHP2KHZ gain1=2.131438840232277;
REALHP2KHZ xyv1[]={0,0,0,0,0,0,0,0,0};

REALHP2KHZ HP2000(REALHP2KHZ v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REALHP2KHZ out=v/gain1;
	for (i=8; i>0; i--) {xyv1[i]=xyv1[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv1[xp]=out;
		for(i=0; i<len; i++) { out+=xyv1[xp+len-i]*biquadb1[bqp+i]-xyv1[yp+len-i]*biquada1[bqp+i]; }
		bqp+=len;
		xyv1[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}


//LP1500
typedef double REALLP1_5KHZ;
#define NBQ 2
REALLP1_5KHZ biquada2[]={0.7261703276714218,-1.5708756060309208,0.44615578450534266,-1.3160525401131666};
REALLP1_5KHZ biquadb2[]={1,2,1,2};
REALLP1_5KHZ gain2=791.9087859193477;
REALLP1_5KHZ xyv2[]={0,0,0,0,0,0,0,0,0};

REALLP1_5KHZ LP1500(REALLP1_5KHZ v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REALLP1_5KHZ out=v/gain2;
	for (i=8; i>0; i--) {xyv2[i]=xyv2[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv2[xp]=out;
		for(i=0; i<len; i++) { out+=xyv2[xp+len-i]*biquadb2[bqp+i]-xyv2[yp+len-i]*biquada2[bqp+i]; }
		bqp+=len;
		xyv2[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}
//LP3500
typedef double REAL3_5;
#define NBQ 2
REAL3_5 biquada3[]={0.5134729320145883,-0.8211290847264716,0.12607365389769115,-0.6109470537598567};
REAL3_5 biquadb3[]={1,2,1,2};
REAL3_5 gain3=44.86257206774139;
REAL3_5 xyv3[]={0,0,0,0,0,0,0,0,0};

REAL3_5 LP3500(REAL3_5 v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REAL3_5 out=v/gain3;
	for (i=8; i>0; i--) {xyv3[i]=xyv3[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv3[xp]=out;
		for(i=0; i<len; i++) { out+=xyv3[xp+len-i]*biquadb3[bqp+i]-xyv3[yp+len-i]*biquada3[bqp+i]; }
		bqp+=len;
		xyv3[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}


//HP for frequency over 5khz (nach s filtern: signal verstärken) 
//wenn beim s filter etwas ist, ist bei den anderen gar nichts (3-5 khz)
typedef double REAL7;
#define NBQ 2
REAL7 biquada4[]={0.4702495216014936,-0.4953121457823088,0.06959119094409258,-0.3603344195067654};
REAL7 biquadb4[]={1,-2,1,-2};
REAL7 gain4=5.692720535890944;
REAL7 xyv4[]={0,0,0,0,0,0,0,0,0};

REAL7 hps(REAL7 v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REAL7 out=v/gain4;
	for (i=8; i>0; i--) {xyv4[i]=xyv4[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv4[xp]=out;
		for(i=0; i<len; i++) { out+=xyv4[xp+len-i]*biquadb4[bqp+i]-xyv4[yp+len-i]*biquada4[bqp+i]; }
		bqp+=len;
		xyv4[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}


//bandpass a

typedef double REALBPA;
#define NBQ1 4
REALBPA biquada5[]={0.9636962520327693,-1.929826432307297,0.9001540798567016,-1.8567954442275394,0.8786659717689861,-1.813513217012141,0.9419500725810619,-1.85310403632223};
REALBPA biquadb5[]={1,-2,1,-2,1,2,1,2};
REALBPA gain5=72902.64293684592;
REALBPA xyv5[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

REALBPA bandpassA(REALBPA v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REALBPA out=v/gain5;
	for (i=14; i>0; i--) {xyv5[i]=xyv5[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv5[xp]=out;
		for(i=0; i<len; i++) { out+=xyv5[xp+len-i]*biquadb5[bqp+i]-xyv5[yp+len-i]*biquada5[bqp+i]; }
		bqp+=len;
		xyv5[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}

//bandpass I
typedef double REAL6;
#define NBQ1 4
REAL6 biquada6[]={0.9021212323939611,-1.849933877930271,0.6863037343826216,-1.6047865384239213,0.49755399504013503,-1.246761149903207,0.7338760770365642,-1.2485206034863805};
REAL6 biquadb6[]={1,-2,1,-2,1,2,1,2};
REAL6 gain6=307.18336286702555;
REAL6 xyv6[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

REAL6 bandpassI(REAL6 v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REAL6 out=v/gain6;
	for (i=14; i>0; i--) {xyv6[i]=xyv6[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv6[xp]=out;
		for(i=0; i<len; i++) { out+=xyv6[xp+len-i]*biquadb6[bqp+i]-xyv6[yp+len-i]*biquada6[bqp+i]; }
		bqp+=len;
		xyv6[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}
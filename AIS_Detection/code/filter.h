typedef double REAL;
#define NBQ 2
REAL xyv[]={0,0,0,0,0,0,0,0,0};

REAL applyfilter(REAL v, int16_t preset)
{
	REAL biquada[4]={0,0,0,0};
	REAL biquadb[4]={0,0,0,0};
	REAL gain=0;
	if( preset == 1){
		//Man A, Cutoff: 730 Hz, width: 100 Hz
		biquada[0] = 0.9835781848181744;
		biquada[1] = -1.9548991443005428;
		biquada[2] = 0.9819320874122156;
		biquada[3] = -1.947132229495664;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=6724.453312737981;

	}
	if (preset == 2){
		//woman A, Cutoff: 850 Hz, width: 100 Hz
		biquada[0] = 0.9834684293158923;
		biquada[1] = -1.9440319084303503;
		biquada[2] = 0.9820627820893103;
		biquada[3] = -1.9355171545283592;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=6652.112391604954;
		
	}
	if (preset == 3){
		//Child A, 1030Hz, Width: 100
		biquada[0] = 0.9833499991378313;
		biquada[1] = -1.9246203438746412;
		biquada[2] = 0.982199954481513;
		biquada[3] = -1.9148895759748172;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=6609.455993094924;
	}

	if (preset == 4){

	
	}
	if (preset == 5){

	}
	if (preset == 6){

	}
	if( preset == 7){
		//Man I, F2, 2290 Hz, Width: 300
		biquada[0] = 0.9512270752259476;
		biquada[1] = -1.6781880566897773;
		biquada[2] = 0.9471116701381416;
		biquada[3] = -1.6204882514641183;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=773.4728330526677;
	}
	if (preset == 8){

		//Woman I, 2790 Hz, width:300Hz	
		biquada[0] = 0.9507873232624283;
		biquada[1] = -1.5431845361483636;
		biquada[2] = 0.947619782741236;
		biquada[3] = -1.476428454172835;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;

		gain=774.5425204751272;
		
	}
	if (preset == 9){

	//child I, 3200 Hz, width 300
	biquada[0] = 0.9505125837156018;
	biquada[1] = -1.4151726608971535;
	biquada[2] = 0.94792869683896;
	biquada[3] = -1.3414824645753887;

	biquadb[0] = 1;
	biquadb[1] = -2;
	biquadb[2] = 1;
	biquadb[3] = 2;
	gain=775.0776980945631;
		
	}
	if (preset == 10){
		//Man I, F1, 270, width = 100
		biquada[0] = 0.9848467750609778;
		biquada[1] = -1.9816437902944406;
		biquada[2] = 0.9801484577712798;
		biquada[3] = -1.9746350902484386;

		biquadb[0] = 1;
		biquadb[1] = -2;
		biquadb[2] = 1;
		biquadb[3] = 2;
		gain=775.0776980945631;
	}
	if (preset == 11){

	}
	if (preset == 12){
		
	}
	int i,b,xp=0,yp=3,bqp=0;
	REAL out=v/gain;
	for (i=8; i>0; i--) {xyv[i]=xyv[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv[xp]=out;
		for(i=0; i<len; i++) { out+=xyv[xp+len-i]*biquadb[bqp+i]-xyv[yp+len-i]*biquada[bqp+i]; }
		bqp+=len;
		xyv[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}

//bandpass I
typedef double REAL6;
#define NBQ1 4
REAL6 biquada6[]={0.9021212323939611,-1.849933877930271,0.6863037343826216,-1.6047865384239213,0.49755399504013503,-1.246761149903207,0.7338760770365642,-1.2485206034863805};
REAL6 biquadb6[]={1,-2,1,-2,1,2,1,2};
REAL6 gain6=307.18336286702555;
REAL6 xyv6[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

REAL6 bandpassI(REAL6 v)
{
	int i,b,xp=0,yp=3,bqp=0;
	REAL6 out=v/gain6;
	for (i=14; i>0; i--) {xyv6[i]=xyv6[i-1];}
	for (b=0; b<NBQ; b++)
	{
		int len=2;
		xyv6[xp]=out;
		for(i=0; i<len; i++) { out+=xyv6[xp+len-i]*biquadb6[bqp+i]-xyv6[yp+len-i]*biquada6[bqp+i]; }
		bqp+=len;
		xyv6[yp]=out;
		xp=yp; yp+=len+1;
	}
	return out;
}

//HPF
const float a0 = 0.9862117951198142;
const float a1 = -1.9724235902396283;
const float a2 = a0;
const float b1 = -1.972233470205696;
const float b2 = 0.9726137102735608;

void HPF(int16_t audio_1[], int16_t audio_2[], uint32_t length) {
  for(int t=0; t<512; t++) audio_1[t] = t * audio_1[t] / 512; // 20ms fade in
  float DELAY[2] = {0.0, 0.0};
  float out;
  audio_2[0] = audio_1[0];
  audio_2[1] = audio_1[1];
  for(int t=2; t<audio_length[1]; t++) {
    out  = a0 * audio_1[t] + a1 * audio_1[t-1] + a2 * audio_1[t-2];
    out -=                   b1 * DELAY[0]     + b2 * DELAY[1];
    DELAY[1] = DELAY[0];
    DELAY[0] = out;
    audio_2[t] = (int)(out + 0.5);
  }
  for(int t=0; t<700; t++) audio_2[t] = 0; // kill artefacts
}